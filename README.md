# Configuração

Adicione no PATH e/ou crie alias dos comandos.

Exemplo:

Adicione no .bashrc ou .zshrc

    export SCRIPTS_UTIL="~/scripts-util"
    alias kube-logs="$SCRIPTS_UTIL/kubernetes/kube-logs"

Para utilizar só executar:

    $ kube-logs app-pod app-namespace # nome do pod e namespace