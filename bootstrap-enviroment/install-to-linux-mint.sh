#!/bin/bash

echo "Para configurar o git"
echo "Qual o seu nome?"
read MY_NAME

echo "Qual seu email?"
read MY_EMAIL

echo "Iniciando a instalação de pricipais ferramentas"
echo "Dados da distribuição"
cat /etc/lsb-release


# UPDATE
sudo apt-get update -y 
sudo apt-get install -y git curl snapd zsh build-essential


# git
git config --global user.email $MY_EMAIL
git config --global user.name $MY_NAME

# SSH 
echo "Gerar sua chave ssh"
ssh-keygen -o -t rsa -C $MY_EMAIL -b 4096

# ZSH / oh-my-zsh
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
sudo chsh -s /bin/zsh


sudo apt-get install -y nmap bluetooth bluez bluez-tools rfkill blueman gparted virtualbox tilix

# NVM
curl -o- https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash

cat <<EOT >> ~/.zshrc

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

EOT

source  ~/.zshrc

# node 8
nvm install 8
# Modules
npm -g gulp httpserver ionic cordova @angular/cli nodemon typescript ts-node

# Snap install 
sudo snap install vscode --classic
# sudo snap install google-cloud-sdk --classic
# sudo snap install kubectl --classic
sudo snap install insomnia
sudo snap install aws-cli --classic
sudo snap install eclipse --classic
sudo snap install slack --classic
sudo snap install skype --classic
sudo snap install remmina

# GCLOUD
wget https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-211.0.0-linux-x86_64.tar.gz
tar zxvf google-cloud-sdk*.tar.gz google-cloud-sdk
./google-cloud-sdk/install.sh

# CHROME 
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add - 
sudo sh -c 'echo "deb https://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
sudo apt-get update
sudo apt-get install google-chrome-stable

# JAVA
sudo add-apt-repository ppa:webupd8team/java -y
sudo apt update -y
sudo apt install oracle-java8-installer -y

# DOCKER
curl -fsSL https://get.docker.com | sh
sudo usermod -aG docker $USER
sudo curl -L \"https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)\" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# AWS - ECS-CLI 
sudo curl -o /usr/local/bin/ecs-cli https://s3.amazonaws.com/amazon-ecs-cli/ecs-cli-linux-amd64-latest
sudo chmod +x /usr/local/bin/ecs-cli

# Snap install extras
sudo snap install gimp
sudo snap install ghex-udt
sudo snap install easy-disk-cleaner
sudo snap install pycharm-community --classic
sudo snap install intellij-idea-community --classic


git clone https://gitlab.com/jsolemos/scripts-util

# Variaveis de ambiente
cat <<EOT >> ~/.zshrc

export ANDROID_HOME="$HOME/Android/Sdk"
export JAVA_HOME="/usr/lib/jvm/java-8-oracle"
export GRADLE_HOME=/opt/android-studio/gradle/gradle-4.1
export PATH=$JAVA_HOME/bin:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$GRADLE_HOME/bin:$ANDROID_HOME/emulator/:$PATH
export MPLBACKEND=TkAgg
export REACT_EDITOR=xed
export SCRIPTS_UTIL="$HOME/scripts-util"
export PATH="$PATH:$SCRIPTS_UTIL/kubernetes:$SCRIPTS_UTIL/docker"

EOT


# Android Studio
curl -o- https://gist.githubusercontent.com/bmc08gt/8077443/raw/48873eed88a015956c13681548385e4a01b6acbd/setup_android_studio.sh | bash



